package com.atlassian.bamboo.plugins.variable.updater;

import com.atlassian.bamboo.ResultKey;
import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.plugins.variable.updater.message.UpdatePlanVariableMessage;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.v2.build.agent.remote.sender.BambooAgentMessageSender;
import com.atlassian.bamboo.variable.VariableContext;
import com.atlassian.bamboo.variable.VariableDefinition;
import com.atlassian.bamboo.variable.VariableDefinitionContext;
import com.atlassian.bamboo.variable.VariableDefinitionManager;
import com.atlassian.spring.container.ContainerManager;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.List;
import java.util.Map;

public class VariableUpdater
{
    private final String buildDirectory;
    private final Map<String, String> customVariables;
    private final ResultKey resultKey;

    public enum SCOPE { JOB, PLAN, RESULT };
    public enum INCREMENT_STRATEGY { REGEX, MAVENRELEASE, NONE, DEPLOYMENT }
    public enum SAVE_STRATEGY { READ_SAVE, SKIP};

    public VariableUpdater(final ResultKey buildResultKey, final String buildDirectory, Map<String, String> customVariables)
    {
        this.buildDirectory = buildDirectory;
        this.customVariables = customVariables;
        this.resultKey = buildResultKey;
    }

    static List<VariableDefinition> retrievePlanGlobalVariables(final VariableDefinitionManager variableDefinitionManager, final PlanManager planManager,
                                                                final String topLevelPlanKey,
                                                                final String variableName, final boolean includeGlobals, boolean useBranchVariables){
        final List<VariableDefinition> variablesToChange = Lists.newArrayList();
        try {
            ImmutablePlan planByKey = planManager.getPlanByKey(PlanKeys.getPlanKey(topLevelPlanKey));

            // it's a branch, but we are not using branch variables
            final boolean isBranch = planByKey.hasMaster() ;
            if (isBranch && !useBranchVariables) {
                planByKey = planByKey.getMaster();
            }
            CollectionUtils.addIgnoreNull(variablesToChange, variableDefinitionManager.getPlanVariableByKey(planByKey, variableName));

            // Branch doesn't have the override of the variable, falling back to master
            if (variablesToChange.isEmpty() && isBranch && useBranchVariables){
                CollectionUtils.addIgnoreNull(variablesToChange, variableDefinitionManager.getPlanVariableByKey(planByKey.getMaster(), variableName));
            }
        } catch (IllegalArgumentException e) { return Lists.newArrayList();}

        if (includeGlobals) {
            CollectionUtils.addIgnoreNull(variablesToChange, variableDefinitionManager.getGlobalVariableByKey(variableName));
        }
        return variablesToChange;
    }

    static List<VariableDefinition> retrieveDeploymentGlobalVariables( final VariableDefinitionManager variableDefinitionManager,
                                                               final String deploymentEnvironmentId,
                                                               final String variableName, final boolean includeGlobals){
        final Long deploymentId;
        try{
            deploymentId = Long.parseLong(deploymentEnvironmentId);
        }  catch (NumberFormatException e){ return Lists.newArrayList(); }

        final List<VariableDefinition> variablesToChange = Lists.newArrayList();
        if (includeGlobals) {
            CollectionUtils.addIgnoreNull(variablesToChange, variableDefinitionManager.getGlobalVariableByKey(variableName));

        }

        final List<VariableDefinition> envVariables = variableDefinitionManager.getDeploymentEnvironmentVariables(deploymentId);
        Iterables.addAll(variablesToChange, Iterables.filter(envVariables, new Predicate<VariableDefinition>() {
            @Override
            public boolean apply(@Nullable VariableDefinition variableDefinition) {
                return variableName.equals(variableDefinition.getKey());
            }
        }));
        return variablesToChange;

    }

    void savePlanVariable(final String planKey, final String variableName, final boolean includeGlobals, final String newValue, boolean isRunningRemote, final BuildLogger buildLogger, final boolean useBranchVariables) {
        if(!isRunningRemote)
        {
            savePlanVariableServerSide(resultKey, planKey, variableName, newValue, includeGlobals, useBranchVariables);
        }
        else {
            BambooAgentMessageSender bambooAgentMessageSender = (BambooAgentMessageSender) ContainerManager.getComponent("bambooAgentMessageSender");
            bambooAgentMessageSender.send(new UpdatePlanVariableMessage(resultKey, planKey, variableName, newValue, includeGlobals, useBranchVariables));
        }
    }

    public static void savePlanVariableServerSide(ResultKey resultKey, String planKeyOrEnvironmentId, String variableName, String newValue, boolean includeGlobals, boolean useBranchVariables)
    {
        VariableDefinitionManager variableDefinitionManager = ContainerManager.getComponent("variableDefinitionManager", VariableDefinitionManager.class);
        final BuildLoggerManager buildLoggerManager = (BuildLoggerManager) ContainerManager.getComponent("buildLoggerManager");
        final PlanManager planManager = (PlanManager) ContainerManager.getComponent("planManager");
        BuildLogger buildLogger = buildLoggerManager.getLogger(resultKey);
        final List<VariableDefinition> planVariablesToChange = retrievePlanGlobalVariables(variableDefinitionManager, planManager,
                planKeyOrEnvironmentId, variableName, includeGlobals, useBranchVariables);
        planVariablesToChange.addAll(retrieveDeploymentGlobalVariables(variableDefinitionManager, planKeyOrEnvironmentId, variableName, includeGlobals));

        for (final VariableDefinition variableDefinition : planVariablesToChange) {
            if (!variableDefinition.getValue().equals(newValue)) {
                variableDefinition.setValue(newValue);
                variableDefinitionManager.saveVariableDefinition(variableDefinition);
                buildLogger.addBuildLogEntry(String.format("Set %s level variable %s to new value %s", variableDefinition.getVariableType(), variableDefinition.getKey(), newValue));

            } else {
                buildLogger.addBuildLogEntry(String.format("Not touching variable %s of type %s, as it's already %s", variableDefinition.getKey(),
                        variableDefinition.getVariableType().name(), newValue));
            }
        }

        if (planVariablesToChange.isEmpty()){
            buildLogger.addErrorLogEntry(String.format("It wasn't possible to retrieve any PLAN scoped variable %s", variableName));
            throw new IllegalArgumentException("No such variable " + variableName);
        }
    }

    void saveJobOrResultVariable(final String variableName, final String newValue, final BuildLogger buildLogger, VariableContext variableContext, final SCOPE scope) {
        Optional<VariableDefinitionContext> variableDefinitionContext = Iterables.tryFind(variableContext.getEffectiveVariables().values(), new Predicate<VariableDefinitionContext>() {
            @Override
            public boolean apply(@javax.annotation.Nullable VariableDefinitionContext input) {
                return StringUtils.equals(variableName, input.getKey());
            }
        });
        String oldValue = variableDefinitionContext.isPresent()? variableDefinitionContext.get().getValue() : null;

        if(scope == SCOPE.JOB) {
            customVariables.put(variableName, newValue);
            buildLogger.addBuildLogEntry(String.format("Changing variable %s of type JOB from %s to %s", variableName,
                    oldValue, newValue));
        }
        else if(scope == SCOPE.RESULT)
        {
            variableContext.addResultVariable(variableName, newValue);
            buildLogger.addBuildLogEntry(String.format("Changing variable %s of type RESULT from %s to %s", variableName,
                    oldValue, newValue));
        }
        else
        {
            throw new IllegalArgumentException("This method can only save variables whose scope is JOB or RESULT, not " + scope);
        }
    }

    public void update(final String planKeyOrEnvironemntId, final String variableName, final String currentValue, final String valueRegex, final boolean includeGlobals,
                       SCOPE variableScope, final INCREMENT_STRATEGY strategy, boolean isRemote, final BuildLogger buildLogger, VariableContext vairableContext, final boolean useBranchVariables) throws TaskException {
        final String newValue;
        try {
            newValue  = new VersionAdvancer().advanceVariable(strategy, currentValue, valueRegex);
        } catch (Exception e) {
            buildLogger.addErrorLogEntry("Unable to advance " + variableName, e);
            throw new TaskException("Failed to increase variable: ", e);
        }

        update(planKeyOrEnvironemntId, variableName, newValue, includeGlobals, variableScope,
                isRemote, buildLogger, vairableContext, useBranchVariables);

    }

    public void update(final String planKeyOrEnvironmentId, final String variableName, final String newValue,
                       final boolean includeGlobals, final SCOPE variableScope,
                       final boolean isRemote, final BuildLogger buildLogger, VariableContext variableContext, final boolean useBranchVariables) throws TaskException {
        try {
            if (variableScope == SCOPE.JOB || variableScope == SCOPE.RESULT) {
                saveJobOrResultVariable(variableName, newValue, buildLogger, variableContext, variableScope);
            } else {
                savePlanVariable(planKeyOrEnvironmentId, variableName, includeGlobals, newValue, isRemote, buildLogger, useBranchVariables);
            }
        } catch (Exception e) {
            buildLogger.addErrorLogEntry("Unable to save " + variableName, e);
            throw new TaskException("Failed to save variable: ", e);
        }
    }

    public void readFromPom(final String topLevelPlanKey, final String variableName, final String currentValue,
                            final boolean includeGlobals, final SCOPE variableScope, final SAVE_STRATEGY strategy,
                            boolean removeSnapshot, String pomFile,
                            boolean isRunningOnRemote,
                            BuildLogger buildLogger, VariableContext variableContext, final boolean useBranchVariables) throws TaskException {
        try {

            final String newValue;
            if (strategy == SAVE_STRATEGY.READ_SAVE){
                final String pomVersion = new PomReader(buildLogger, new File(buildDirectory), pomFile).retrieveValue();
                newValue = removeSnapshot? pomVersion.replace("-SNAPSHOT", "") : pomVersion;
            } else {
                newValue = currentValue;
            }

            if(SCOPE.JOB == variableScope || SCOPE.RESULT == variableScope){
                saveJobOrResultVariable(variableName, newValue, buildLogger, variableContext, variableScope);
            } else {
                savePlanVariable(topLevelPlanKey, variableName, includeGlobals, newValue, isRunningOnRemote, buildLogger, useBranchVariables);
            }

        } catch (Exception e) {
            buildLogger.addErrorLogEntry(e.getMessage(), e);
            throw new TaskException("Failed to read variable: ", e);
        }
    }

    public void readFromFile(final String topLevelPlanKey, final String variableName, final String currentValue,
                             final boolean includeGlobals, final SCOPE variableScope, final SAVE_STRATEGY strategy,
                             final String filename,
                             boolean isRunningOnRemote,
                             BuildLogger buildLogger, VariableContext variableContext, final boolean useBranchVariables) throws TaskException {
        try {

            final String newValue;
            if (strategy == SAVE_STRATEGY.READ_SAVE){
                List<String> list = FileUtils.readLines(new File(buildDirectory, filename));
                newValue = StringUtils.join(list, "\n");
            } else {
                newValue = currentValue;
            }

            if(SCOPE.JOB == variableScope || SCOPE.RESULT == variableScope){
                saveJobOrResultVariable(variableName, newValue, buildLogger, variableContext, variableScope);
            }
            else {
                savePlanVariable(topLevelPlanKey, variableName, includeGlobals, newValue, isRunningOnRemote, buildLogger, useBranchVariables);
            }

        } catch (Exception e) {
            buildLogger.addErrorLogEntry(e.getMessage(), e);
            throw new TaskException("Failed to read variable: ", e);
        }
    }
}
