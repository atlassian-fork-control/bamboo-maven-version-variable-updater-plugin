package com.atlassian.bamboo.plugins.variable.updater;


import org.apache.maven.shared.release.versions.DefaultVersionInfo;
import org.apache.maven.shared.release.versions.VersionParseException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VersionAdvancer {
    public String advanceVariable(VariableUpdater.INCREMENT_STRATEGY strategy, String value, String regex) throws VersionParseException {
        String newValue;
        switch (strategy){
          case MAVENRELEASE: newValue = new MavenVersionAdvancer().advance(value); break;
          case REGEX: newValue = new RegexVersionAdvancer().advance(value, Pattern.compile(regex)); break;
          case DEPLOYMENT: newValue = new BambooVersionAdvancer().advance(value); break;
          case NONE: newValue = value; break;
          default: throw new IllegalArgumentException();
        }
        return newValue;
    }

}

class MavenVersionAdvancer
{
    public String advance(String version) throws VersionParseException
    {
        final DefaultVersionInfo defaultVersionInfo = new DefaultVersionInfo(version);
        return defaultVersionInfo.getNextVersion().toString();
    }
}

class BambooVersionAdvancer
{
    /**
     * Copy logic from bamboo code. Because in a remote agent, deploymentVersionService cannot be accessed
     * @param version
     * @return
     * @throws VersionParseException
     */
    public String advance(final String version) throws VersionParseException
    {
        char[] originalChars = version.toCharArray();

        boolean inVariable = false;
        boolean inNumber = false;
        int numberEndIndex = -1;
        int numberStartIndex = 0;

        for (int i = originalChars.length - 1; i >= 0; i--)
        {
            char c = originalChars[i];
            if (c >= '0' && c <= '9')
            {
                if (!inVariable && !inNumber)
                {
                    inNumber = true;
                    numberEndIndex = i;
                }
            }
            else
            {
                if (inNumber)
                {
                    numberStartIndex = i + 1; // technically c is the letter before number, so have to +1
                    break;
                }

                if (c == '}')
                {
                    inVariable = true;
                }
                else if (c == '{')
                {
                    inVariable = false;
                }
            }
        }

        if (numberEndIndex >= 0)
        {
            String oldVersionNumber = version.substring(numberStartIndex, numberEndIndex + 1);
            long numberToIncrement = Long.parseLong(oldVersionNumber);
            numberToIncrement++;

            StringBuilder newString = new StringBuilder();
            newString.append(version.substring(0, numberStartIndex));
            newString.append(numberToIncrement);
            newString.append(version.substring(numberEndIndex + 1));
            return newString.toString();
        }

        // no numbers to increment lets just return original name
        return version;
    }
}

class RegexVersionAdvancer {
    public String advance(String versionString, Pattern regex)
    {
        final Matcher matcher = regex.matcher(versionString);
        if (matcher.find() && matcher.groupCount() == 1)
        {
            int start = matcher.start(1);
            int end = matcher.end(1);
            long version = Long.parseLong(matcher.group(1));
            long newVersion = version + 1;

            StringBuilder builder = new StringBuilder();
            builder.append(versionString.substring(0, start));
            builder.append(String.valueOf(newVersion));
            builder.append(versionString.substring(end, versionString.length()));
            return builder.toString();
        }
        return versionString;
    }
}