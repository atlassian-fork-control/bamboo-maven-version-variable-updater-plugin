package com.atlassian.bamboo.plugins.variable.updater.task.configuration;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.variable.updater.VariableUpdater;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.regex.Pattern;

import static com.atlassian.bamboo.plugins.variable.updater.task.executor.AdvanceVersionVariableTask.UPDATE_STRATEGY_CONFIG_KEY;
import static com.atlassian.bamboo.plugins.variable.updater.task.executor.AdvanceVersionVariableTask.VERSION_PATTERN_TO_MATCH_CONFIG_KEY;

public class AdvanceVersionVariableTaskConfigurator extends VersionVariableTaskConfigurator
{
    void populateLists(@NotNull final Map<String, Object> context) {
        super.populateLists(context);
        final ImmutableMap<String, String> strategyOptions =
                new ImmutableMap.Builder<String, String>()
                        .put(VariableUpdater.INCREMENT_STRATEGY.REGEX.name(), getI18nBean().getText("com.atlassian.bamboo.plugins.variable.updater.value.regex"))
                        .put(VariableUpdater.INCREMENT_STRATEGY.MAVENRELEASE.name(), getI18nBean().getText("com.atlassian.bamboo.plugins.variable.updater.value.maven.release"))
                        .put(VariableUpdater.INCREMENT_STRATEGY.DEPLOYMENT.name(), getI18nBean().getText("com.atlassian.bamboo.plugins.variable.updater.value.bamboo.release"))
                        .build();

        context.put("strategyOptions", strategyOptions);
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put(VERSION_PATTERN_TO_MATCH_CONFIG_KEY, params.getString(VERSION_PATTERN_TO_MATCH_CONFIG_KEY));
        config.put(UPDATE_STRATEGY_CONFIG_KEY, params.getString(UPDATE_STRATEGY_CONFIG_KEY));
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        context.put(VERSION_PATTERN_TO_MATCH_CONFIG_KEY, "");
        context.put(UPDATE_STRATEGY_CONFIG_KEY, VariableUpdater.INCREMENT_STRATEGY.MAVENRELEASE);
        populateLists(context);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        context.put(VERSION_PATTERN_TO_MATCH_CONFIG_KEY, taskDefinition.getConfiguration().get(VERSION_PATTERN_TO_MATCH_CONFIG_KEY));
        context.put(UPDATE_STRATEGY_CONFIG_KEY, taskDefinition.getConfiguration().get(UPDATE_STRATEGY_CONFIG_KEY));
        populateLists(context);
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForView(context, taskDefinition);
        context.put(VERSION_PATTERN_TO_MATCH_CONFIG_KEY, taskDefinition.getConfiguration().get(VERSION_PATTERN_TO_MATCH_CONFIG_KEY));
        context.put(UPDATE_STRATEGY_CONFIG_KEY, taskDefinition.getConfiguration().get(UPDATE_STRATEGY_CONFIG_KEY));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);

        if (!StringUtils.isBlank(params.getString(VERSION_PATTERN_TO_MATCH_CONFIG_KEY)))
        {
            try
            {
                Pattern.compile(params.getString(VERSION_PATTERN_TO_MATCH_CONFIG_KEY));
            }
            catch (Exception e)
            {
                errorCollection.addError(VERSION_PATTERN_TO_MATCH_CONFIG_KEY, getI18nBean().getText("com.atlassian.bamboo.plugins.variable.updater.value.invalid.error"));
            }
        }
    }
}
