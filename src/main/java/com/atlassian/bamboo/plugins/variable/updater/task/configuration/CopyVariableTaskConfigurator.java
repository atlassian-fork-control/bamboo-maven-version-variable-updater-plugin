package com.atlassian.bamboo.plugins.variable.updater.task.configuration;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import static com.atlassian.bamboo.plugins.variable.updater.task.executor.CopyVariableTask.ORIGIN_VARIABLE_CONFIG;

public class CopyVariableTaskConfigurator extends VersionVariableTaskConfigurator
{

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put(ORIGIN_VARIABLE_CONFIG, params.getString(ORIGIN_VARIABLE_CONFIG));
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        context.put(ORIGIN_VARIABLE_CONFIG, "");
        populateLists(context);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        context.put(ORIGIN_VARIABLE_CONFIG, taskDefinition.getConfiguration().get(ORIGIN_VARIABLE_CONFIG));
        populateLists(context);
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForView(context, taskDefinition);
        context.put(ORIGIN_VARIABLE_CONFIG, taskDefinition.getConfiguration().get(ORIGIN_VARIABLE_CONFIG));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        if (params.getString(ORIGIN_VARIABLE_CONFIG) == null || params.getString(ORIGIN_VARIABLE_CONFIG).isEmpty()){
            errorCollection.addError(ORIGIN_VARIABLE_CONFIG, getI18nBean().getText("com.atlassian.bamboo.plugins.variable.updater.empty.error"));
        }
    }
}
