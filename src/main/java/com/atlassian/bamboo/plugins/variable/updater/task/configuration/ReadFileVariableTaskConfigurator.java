package com.atlassian.bamboo.plugins.variable.updater.task.configuration;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import static com.atlassian.bamboo.plugins.variable.updater.task.executor.ReadFileVariableTask.FILENAME_CONFIG_KEY;

public class ReadFileVariableTaskConfigurator extends VersionVariableTaskConfigurator
{

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put(FILENAME_CONFIG_KEY, params.getString(FILENAME_CONFIG_KEY));
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        context.put(FILENAME_CONFIG_KEY, "");
        populateLists(context);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        context.put(FILENAME_CONFIG_KEY, taskDefinition.getConfiguration().get(FILENAME_CONFIG_KEY));
        populateLists(context);
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForView(context, taskDefinition);
        context.put(FILENAME_CONFIG_KEY, taskDefinition.getConfiguration().get(FILENAME_CONFIG_KEY));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        if (params.getString(FILENAME_CONFIG_KEY) == null || params.getString(FILENAME_CONFIG_KEY).isEmpty()){
            errorCollection.addError(FILENAME_CONFIG_KEY, getI18nBean().getText("com.atlassian.bamboo.plugins.variable.updater.empty.error"));
        }
    }
}
