package com.atlassian.bamboo.plugins.variable.updater.task.configuration;


import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.TaskDefinition;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import static com.atlassian.bamboo.plugins.variable.updater.task.executor.ExtractVersionVariableTask.POM_FILE_OVERRIDE;
import static com.atlassian.bamboo.plugins.variable.updater.task.executor.ExtractVersionVariableTask.REMOVE_SNAPSHOT;

public class ExtractVersionVariableTaskConfigurator extends VersionVariableTaskConfigurator{
    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put(POM_FILE_OVERRIDE, params.getString(POM_FILE_OVERRIDE));
        config.put(REMOVE_SNAPSHOT, params.getString(REMOVE_SNAPSHOT));
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        context.put(POM_FILE_OVERRIDE, "");
        context.put(REMOVE_SNAPSHOT, "false");
        populateLists(context);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        context.put(POM_FILE_OVERRIDE, taskDefinition.getConfiguration().get(POM_FILE_OVERRIDE));
        context.put(REMOVE_SNAPSHOT, taskDefinition.getConfiguration().get(REMOVE_SNAPSHOT));
        populateLists(context);
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForView(context, taskDefinition);
        context.put(POM_FILE_OVERRIDE, taskDefinition.getConfiguration().get(POM_FILE_OVERRIDE));
        context.put(REMOVE_SNAPSHOT, taskDefinition.getConfiguration().get(REMOVE_SNAPSHOT));
    }
}
