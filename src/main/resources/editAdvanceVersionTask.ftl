[@ui.bambooSection titleKey="com.atlassian.bamboo.plugins.variable.updater.variable.title"]
    [@ww.textfield labelKey="com.atlassian.bamboo.plugins.variable.updater.variable.name" name="variable"
    required=true cssClass="long-field"/]
    [@ww.checkbox labelKey="com.atlassian.bamboo.plugins.variable.updater.variable.customised" name="overrideCustomised" /]
[/@ui.bambooSection]

[@ui.bambooSection titleKey="com.atlassian.bamboo.plugins.variable.updater.scope.title"]
    [@ww.select labelKey='com.atlassian.bamboo.plugins.variable.updater.scope' name='variableScope'
        listKey='key' listValue='value' toggle='true' required=true
        list="variableScopeOptions" cssClass="long-field"]
    [/@ww.select]
    [@ui.bambooSection dependsOn='variableScope' showOn='PLAN']
        [@ww.checkbox labelKey="com.atlassian.bamboo.plugins.variable.updater.variable.globals" name="includeGlobals" /]
        [@ww.checkbox labelKey="com.atlassian.bamboo.plugins.variable.updater.variable.branch" name="branchVars" /]
    [/@ui.bambooSection]
[/@ui.bambooSection]

[@ui.bambooSection titleKey="com.atlassian.bamboo.plugins.variable.updater.value.title"]
    [@ww.select labelKey='com.atlassian.bamboo.plugins.variable.updater.value' name='strategy'
        listKey='key' listValue='value' toggle='true' required=true
        list="strategyOptions" cssClass="long-field"/]

    [@ui.bambooSection dependsOn='strategy' showOn='REGEX']
        [@ww.textfield labelKey="com.atlassian.bamboo.plugins.variable.updater.value.regex.input" name="versionPattern"
        required=true cssClass="long-field"/]
    [/@ui.bambooSection]

[/@ui.bambooSection]
