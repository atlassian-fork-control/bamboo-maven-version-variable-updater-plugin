package com.atlassian.bamboo.plugins.variable.updater;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.variable.VariableContext;
import com.atlassian.bamboo.variable.VariableDefinition;
import com.atlassian.bamboo.variable.VariableDefinitionContext;
import com.atlassian.bamboo.variable.VariableDefinitionManager;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.hamcrest.Matchers;
import org.hamcrest.collection.IsIterableContainingInAnyOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VariableUpdaterTest {

    @InjectMocks
    private VariableUpdater variableUpdater;
    @Mock
    private PlanManager planManager;
    @Mock
    private VariableDefinitionManager variableDefinitionManager;
    @Mock
    private BuildLogger buildLogger;
    @Mock
    private Map<String, String> customVariables;

    private static final String variable = "my.variable.name";
    private static final String plankey = "PROJ-PLAN";
    private static final String branchkey = "PROJ-PLAN0";
    private static final String deploymentEnvironmentId = "12345";


    @Test
    public void returnsBothGlobalAndPlanVariableWhenPresent() throws Exception {
        final VariableDefinition planVariable = mock(VariableDefinition.class);
        final VariableDefinition globalVariable = mock(VariableDefinition.class);
        final Plan plan = mock(Plan.class);

        when(variableDefinitionManager.getGlobalVariableByKey(variable)).thenReturn(globalVariable);
        when(planManager.getPlanByKey(PlanKeys.getPlanKey(plankey))).thenReturn(plan);
        when(variableDefinitionManager.getPlanVariableByKey(plan, variable)).thenReturn(planVariable);

        final List<VariableDefinition> variables = variableUpdater.retrievePlanGlobalVariables(variableDefinitionManager, planManager, plankey, variable, true, false);

        assertThat(variables,
                IsIterableContainingInAnyOrder.<VariableDefinition>containsInAnyOrder(planVariable, globalVariable));
    }

    @Test
    public void returnsOnlyGlobalForPlans() throws Exception {
        final VariableDefinition planVariable = mock(VariableDefinition.class);
        final VariableDefinition globalVariable = mock(VariableDefinition.class);
        final Plan plan = mock(Plan.class);

        when(variableDefinitionManager.getGlobalVariableByKey(variable)).thenReturn(globalVariable);
        when(planManager.getPlanByKey(PlanKeys.getPlanKey(plankey))).thenReturn(plan);
        when(variableDefinitionManager.getPlanVariableByKey(plan, variable)).thenReturn(null);

        final List<VariableDefinition> variables = variableUpdater.retrievePlanGlobalVariables(variableDefinitionManager, planManager, plankey, variable, true, false);

        assertThat(variables,
                IsIterableContainingInAnyOrder.<VariableDefinition>containsInAnyOrder(globalVariable));
    }

    @Test
    public void returnsOnlyBranchVariableForPlans() throws Exception {
        final VariableDefinition branchVariable = mock(VariableDefinition.class);
        final VariableDefinition planVariable = mock(VariableDefinition.class);
        final VariableDefinition globalVariable = mock(VariableDefinition.class);
        final Plan branchPlan = mock(Plan.class);
        final Plan masterPlan = mock(Plan.class);

        when(variableDefinitionManager.getGlobalVariableByKey(variable)).thenReturn(globalVariable);
        when(planManager.getPlanByKey(PlanKeys.getPlanKey(branchkey))).thenReturn(branchPlan);
        when(branchPlan.hasMaster()).thenReturn(true);
        when(branchPlan.getMaster()).thenReturn(masterPlan);
        when(variableDefinitionManager.getPlanVariableByKey(branchPlan, variable)).thenReturn(branchVariable);
        when(variableDefinitionManager.getPlanVariableByKey(masterPlan, variable)).thenReturn(planVariable);


        final List<VariableDefinition> variables = variableUpdater.retrievePlanGlobalVariables(variableDefinitionManager, planManager, branchkey, variable, false, true);

        assertThat(variables,
                IsIterableContainingInAnyOrder.containsInAnyOrder(branchVariable));
    }

    @Test
    public void returnsMasterVariableForBranchesIfNoOverride() throws Exception {
        final VariableDefinition planVariable = mock(VariableDefinition.class);
        final VariableDefinition globalVariable = mock(VariableDefinition.class);
        final Plan branchPlan = mock(Plan.class);
        final Plan masterPlan = mock(Plan.class);

        when(variableDefinitionManager.getGlobalVariableByKey(variable)).thenReturn(globalVariable);
        when(planManager.getPlanByKey(PlanKeys.getPlanKey(branchkey))).thenReturn(branchPlan);
        when(branchPlan.getMaster()).thenReturn(masterPlan);
        when(branchPlan.hasMaster()).thenReturn(true);
        when(variableDefinitionManager.getPlanVariableByKey(branchPlan, variable)).thenReturn(null);
        when(variableDefinitionManager.getPlanVariableByKey(masterPlan, variable)).thenReturn(planVariable);


        final List<VariableDefinition> variables = variableUpdater.retrievePlanGlobalVariables(variableDefinitionManager, planManager, branchkey, variable, false, true);

        assertThat(variables,
                IsIterableContainingInAnyOrder.containsInAnyOrder(planVariable));
    }

    @Test
    public void returnsMasterVariableIfNotUsingBranchVariables() throws Exception {
        final VariableDefinition branchVariable = mock(VariableDefinition.class);
        final VariableDefinition planVariable = mock(VariableDefinition.class);
        final VariableDefinition globalVariable = mock(VariableDefinition.class);
        final Plan branchPlan = mock(Plan.class);
        final Plan masterPlan = mock(Plan.class);

        when(variableDefinitionManager.getGlobalVariableByKey(variable)).thenReturn(globalVariable);
        when(planManager.getPlanByKey(PlanKeys.getPlanKey(branchkey))).thenReturn(branchPlan);
        when(branchPlan.hasMaster()).thenReturn(true);
        when(branchPlan.getMaster()).thenReturn(masterPlan);
        when(variableDefinitionManager.getPlanVariableByKey(branchPlan, variable)).thenReturn(branchVariable);
        when(variableDefinitionManager.getPlanVariableByKey(masterPlan, variable)).thenReturn(planVariable);


        final List<VariableDefinition> variables = variableUpdater.retrievePlanGlobalVariables(variableDefinitionManager, planManager, branchkey, variable, false, false);

        assertThat(variables,
                IsIterableContainingInAnyOrder.containsInAnyOrder(planVariable));
    }

    @Test
    public void returnsBranchAndGlobalVariables() throws Exception {
        final VariableDefinition branchVariable = mock(VariableDefinition.class);
        final VariableDefinition planVariable = mock(VariableDefinition.class);
        final VariableDefinition globalVariable = mock(VariableDefinition.class);
        final Plan branchPlan = mock(Plan.class);
        final Plan masterPlan = mock(Plan.class);

        when(variableDefinitionManager.getGlobalVariableByKey(variable)).thenReturn(globalVariable);
        when(planManager.getPlanByKey(PlanKeys.getPlanKey(branchkey))).thenReturn(branchPlan);
        when(branchPlan.hasMaster()).thenReturn(true);
        when(branchPlan.getMaster()).thenReturn(masterPlan);
        when(variableDefinitionManager.getPlanVariableByKey(branchPlan, variable)).thenReturn(branchVariable);
        when(variableDefinitionManager.getPlanVariableByKey(masterPlan, variable)).thenReturn(planVariable);


        final List<VariableDefinition> variables = variableUpdater.retrievePlanGlobalVariables(variableDefinitionManager, planManager, branchkey, variable, true, true);

        assertThat(variables,
                IsIterableContainingInAnyOrder.containsInAnyOrder(branchVariable, globalVariable));
    }

    @Test
    public void failsIfOnlyExistBranchVariable() throws Exception {
        final VariableDefinition branchVariable = mock(VariableDefinition.class);
        final Plan branchPlan = mock(Plan.class);
        final Plan masterPlan = mock(Plan.class);

        when(variableDefinitionManager.getGlobalVariableByKey(variable)).thenReturn(null);
        when(planManager.getPlanByKey(PlanKeys.getPlanKey(branchkey))).thenReturn(branchPlan);
        when(branchPlan.hasMaster()).thenReturn(true);
        when(branchPlan.getMaster()).thenReturn(masterPlan);
        when(variableDefinitionManager.getPlanVariableByKey(branchPlan, variable)).thenReturn(branchVariable);
        when(variableDefinitionManager.getPlanVariableByKey(masterPlan, variable)).thenReturn(null);


        final List<VariableDefinition> variables = variableUpdater.retrievePlanGlobalVariables(variableDefinitionManager, planManager, branchkey, variable, true, false);

        assertThat(variables, Matchers.empty());
    }

    @Test
    public void returnsOnlyPlanVariableEvenWhenPresent() throws Exception {
        final VariableDefinition planVariable = mock(VariableDefinition.class);
        final VariableDefinition globalVariable = mock(VariableDefinition.class);
        final Plan plan = mock(Plan.class);

        when(variableDefinitionManager.getGlobalVariableByKey(variable)).thenReturn(globalVariable);
        when(planManager.getPlanByKey(PlanKeys.getPlanKey(plankey))).thenReturn(plan);
        when(variableDefinitionManager.getPlanVariableByKey(plan, variable)).thenReturn(planVariable);

        final List<VariableDefinition> variables = variableUpdater.retrievePlanGlobalVariables(variableDefinitionManager, planManager, plankey, variable, false, false);

        assertThat(variables,
                IsIterableContainingInAnyOrder.<VariableDefinition>containsInAnyOrder(planVariable));
    }

    @Test
    public void returnsOnlyDeploymentVariableEvenWhenPresent() throws Exception {
        final VariableDefinition deploymentVariable = mock(VariableDefinition.class);
        final VariableDefinition globalVariable = mock(VariableDefinition.class);

        when(variableDefinitionManager.getGlobalVariableByKey(variable)).thenReturn(globalVariable);
        when(variableDefinitionManager.getDeploymentEnvironmentVariables(12345L)).thenReturn(Lists.newArrayList(deploymentVariable));
        when(deploymentVariable.getKey()).thenReturn(variable);

        final List<VariableDefinition> variables = variableUpdater.retrieveDeploymentGlobalVariables(variableDefinitionManager, deploymentEnvironmentId, variable, false);

        assertThat(variables,
                IsIterableContainingInAnyOrder.<VariableDefinition>containsInAnyOrder(deploymentVariable));
    }

    @Test
    public void returnsBothGlobalAndDeploymentVariableWhenPresent() throws Exception {
        final VariableDefinition deploymentVariable = mock(VariableDefinition.class);
        final VariableDefinition globalVariable = mock(VariableDefinition.class);

        when(variableDefinitionManager.getGlobalVariableByKey(variable)).thenReturn(globalVariable);
        when(variableDefinitionManager.getDeploymentEnvironmentVariables(12345L)).thenReturn(Lists.newArrayList(deploymentVariable));
        when(deploymentVariable.getKey()).thenReturn(variable);

        final List<VariableDefinition> variables = variableUpdater.retrieveDeploymentGlobalVariables(variableDefinitionManager, deploymentEnvironmentId, variable, true);

        assertThat(variables,
                IsIterableContainingInAnyOrder.<VariableDefinition>containsInAnyOrder(deploymentVariable, globalVariable));

    }

    @Test
    public void returnsOnlyGlobaForDeployments() throws Exception {
        final VariableDefinition globalVariable = mock(VariableDefinition.class);

        when(variableDefinitionManager.getGlobalVariableByKey(variable)).thenReturn(globalVariable);
        when(variableDefinitionManager.getDeploymentEnvironmentVariables(12345L)).thenReturn(Lists.<VariableDefinition>newArrayList());

        final List<VariableDefinition> variables = variableUpdater.retrieveDeploymentGlobalVariables(variableDefinitionManager, deploymentEnvironmentId, variable, true);

        assertThat(variables,
                IsIterableContainingInAnyOrder.<VariableDefinition>containsInAnyOrder(globalVariable));
    }

    @Test
    public void updateJobLevelVariables() throws Exception{
        VariableContext variableContext = mock(VariableContext.class);
        when(variableContext.getEffectiveVariables()).thenReturn(Maps.<String, VariableDefinitionContext>newHashMap());
        BuildLogger buildLogger = mock(BuildLogger.class);
        when(buildLogger.addBuildLogEntry(isA(String.class))).thenReturn("");
        when(customVariables.put(isA(String.class), isA(String.class))).thenReturn("");
        String newValue = "new";
        variableUpdater.update(deploymentEnvironmentId, variable, newValue, false, VariableUpdater.SCOPE.JOB, false, buildLogger, variableContext, false);
        verify(customVariables, times(1)).put(eq(variable), eq(newValue));
    }

    @Test
    public void updateResultLevelVariables() throws Exception{
        VariableContext variableContext = mock(VariableContext.class);
        when(variableContext.getEffectiveVariables()).thenReturn(Maps.<String, VariableDefinitionContext>newHashMap());
        BuildLogger buildLogger = mock(BuildLogger.class);
        when(buildLogger.addBuildLogEntry(isA(String.class))).thenReturn("");
        String newValue = "new";
        variableUpdater.update(deploymentEnvironmentId, variable, newValue, false, VariableUpdater.SCOPE.RESULT, false, buildLogger, variableContext, false);

        verify(variableContext, times(1)).addResultVariable(eq(variable), eq(newValue));
    }
}